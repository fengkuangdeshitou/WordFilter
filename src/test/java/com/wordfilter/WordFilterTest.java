package com.wordfilter;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * WordFilterTest
 * Created by Administrator on 2015/12/29 0029.
 */
public class WordFilterTest {



    @Test
    public void testjudge() {
        WordFilter filter = new WordFilter("sms.dic");
        Set<String> judge = filter.judge("据说，哦！你有点逗比，家用天线约炮，约炮。江泽民和江青黑没有任何关系，薄熙来也是个大美女。");
        for (String word : judge) {
            System.out.println(word);
        }
        System.out.println("据说，哦！你有点逗比，家用天线约炮，约炮。江泽民和江青黑没有任何关系，薄熙来也是个大美女。");
        System.out.println(filter.replace("据说，哦！你有点逗比，家用天线约炮，约炮。江泽民和江青黑没有任何关系，薄熙来也是个大美女。", '-'));
    }

    @Test
    public void testreplace() {
        WordFilter filter = new WordFilter("sms.dic");
        System.out.println(filter.replace("据说，哦！你有点逗比，家用天线约炮，约炮。江泽民和江青黑没有任何关系，薄熙来也是个大美女。", '-'));
    }

    @Test
    public void testdic() throws IOException {
        InputStream input = WordFilter.class.getClassLoader().getResourceAsStream("sms.dic");
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        String line = null;
        List<String> words = new ArrayList<String>();
        while ((line = reader.readLine()) != null) {
            words.add(line);
        }
        Collections.sort(words, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return Integer.compare(o2.length(), o1.length());
            }
        });

        for (String word : words) {
            System.out.println(word);
        }

    }

}